import Vue from 'vue';
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import '@/assets/font_icon/iconfont.css'
import '@/assets/css/base.css'
import App from './App.vue';
import router from './router/index.js'

Vue.config.productionTip = false;
Vue.use(ElementUI);

new Vue({
  render: h => h(App),
  router
}).$mount('#app');
