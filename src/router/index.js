import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '@/views/home.vue';

Vue.use(VueRouter);

const routes = [{ name: 'home', path: '/', component: Home }];

const router = new VueRouter({
  routes // (缩写) 相当于 routes: routes
});

export default router;
